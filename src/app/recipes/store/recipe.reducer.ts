import {RecipeModel} from '../recipe.model';
import * as fromRecipeActions from './recipe.actions';
import {RecipeActionTypes} from './recipe.actions';

export const recipeFeatureKey = 'recipe';

export interface State {
  recipes: RecipeModel[];
}

export const initialState: State = {
  recipes: []
};

export function recipeReducer(state = initialState, action: fromRecipeActions.RecipeActions): State {
  switch (action.type) {

    case RecipeActionTypes.SET_RECIPES:
      return {
        ...state,
        recipes: [...action.payload]
      };

    case RecipeActionTypes.ADD_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes, action.payload]
      };
    case RecipeActionTypes.UPDATE_RECIPE:
      const updateRecipe = {...state.recipes[action.payload.index], ...action.payload.recipe};
      const updatedRecipes = [...state.recipes];
      updatedRecipes[action.payload.index] = updateRecipe;

      return {
        ...state,
        recipes: [...updatedRecipes]
      };
    case RecipeActionTypes.DELETE_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes.filter((value, index) => {
           return index !== action.payload;
        })]
      };

    default:
      return state;
  }
}
