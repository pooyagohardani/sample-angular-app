import {Action} from '@ngrx/store';
import {RecipeModel} from '../recipe.model';

export enum RecipeActionTypes {
  SET_RECIPES = '[Recipe] Set Recipes',
  FETCH_RECIPES = '[Recipe] Fetch Recipes',
  ADD_RECIPE = '[Recipe] Add Recipe',
  UPDATE_RECIPE = '[Recipe] Update Recipe',
  DELETE_RECIPE = '[Recipe] Delete Recipe',
  STORE_RECIPES = '[Recipe] Store Recipe',


}

export class SetRecipesAction implements Action {
  readonly type = RecipeActionTypes.SET_RECIPES;

  constructor(public payload: RecipeModel[]) {
  }
}

export class FetchRecipesAction implements Action {
  readonly type = RecipeActionTypes.FETCH_RECIPES;

  constructor() {
  }
}

export class AddRecipeAction implements Action {
  readonly type = RecipeActionTypes.ADD_RECIPE;

  constructor(public payload: RecipeModel) {
  }
}

export class UpdateRecipeAction implements Action {
  readonly type = RecipeActionTypes.UPDATE_RECIPE;

  constructor(public payload: { index: number, recipe: RecipeModel }) {
  }
}

export class DeleteRecipeAction implements Action {
  readonly type = RecipeActionTypes.DELETE_RECIPE;

  constructor(public payload: number) {
  }
}

export class StoreRecipesAction implements Action {
  readonly type = RecipeActionTypes.STORE_RECIPES;

}


export type RecipeActions =
  SetRecipesAction
  | FetchRecipesAction
  | AddRecipeAction
  | UpdateRecipeAction
  | DeleteRecipeAction
  | StoreRecipesAction
  ;
