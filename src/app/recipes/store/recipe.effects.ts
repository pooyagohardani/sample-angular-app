import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import * as fromRecipeActions from './recipe.actions';
import {RecipeActionTypes} from './recipe.actions';
import {map, switchMap, withLatestFrom} from 'rxjs/operators';
import {RecipeModel} from '../recipe.model';
import {HttpClient} from '@angular/common/http';
import {Store} from '@ngrx/store';
import * as fromApp from '../../app.reducer';


@Injectable()
export class RecipeEffects {

  constructor(
    private actions$: Actions,
    private httpClient: HttpClient,
    private store: Store<fromApp.AppState>) {
  }

  @Effect()
  fetchRecipes = this.actions$.pipe(
    ofType(RecipeActionTypes.FETCH_RECIPES),
    switchMap((fetchRecipesAction: fromRecipeActions.FetchRecipesAction) => {
      return this.httpClient
        .get<RecipeModel[]>('https://ng-course-recipe-book-c5163.firebaseio.com/recipes.json')
        .pipe(
          map(recipes => {
            return recipes.map(recipe => {
              return {...recipe, ingredients: recipe.ingredients ? recipe.ingredients : []};
            });
          }),
          map(recipes => {
            return new fromRecipeActions.SetRecipesAction(recipes);
          })
        );
    })
  );

  @Effect({dispatch: false})
  storeRecipes = this.actions$.pipe(
    ofType(RecipeActionTypes.STORE_RECIPES),
    withLatestFrom(this.store.select('recipes')),
    switchMap(([actionData, recipesState]) => {
      return this.httpClient
        .put(
          'https://ng-course-recipe-book-c5163.firebaseio.com/recipes.json',
          recipesState.recipes
        );
    })
  );


}
