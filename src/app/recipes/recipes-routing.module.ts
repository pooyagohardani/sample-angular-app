import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {RecipesComponent} from './recipes.component';
import {AuthGuardService} from '../auth/auth-guard.service';
import {RecipeStartComponent} from './recipe-start/recipe-start.component';
import {RecipeEditComponent} from './recipe-edit/recipe-edit.component';
import {RecipeDetailComponent} from './recipe-detail/recipe-detail.component';
import {RecipeResolveService} from './recipe-resolve.service';


const routes = [{
  path: '', component: RecipesComponent,
  canActivate: [AuthGuardService],
  children: [
    {path: '', component: RecipeStartComponent},
    {path: 'new', component: RecipeEditComponent},
    {
      path: ':id', component: RecipeDetailComponent,
      resolve: [RecipeResolveService]
    },
    {
      path: ':id/edit', component: RecipeEditComponent,
      resolve: [RecipeResolveService]
    }
  ]
}];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RecipesRoutingModule {
}
