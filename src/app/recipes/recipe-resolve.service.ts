import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {RecipeModel} from './recipe.model';
import {of} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromApp from '../app.reducer';
import * as fromRecipeActions from '../recipes/store/recipe.actions';
import {RecipeActionTypes} from '../recipes/store/recipe.actions';
import {map, switchMap, take} from 'rxjs/operators';
import {Actions, ofType} from '@ngrx/effects';

@Injectable({
  providedIn: 'root'
})
export class RecipeResolveService implements Resolve<RecipeModel[]> {
  constructor(private store: Store<fromApp.AppState>,
              private actions$: Actions) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select('recipes')
      .pipe(
        take(1),
        map(recipeStatus => recipeStatus.recipes),
        switchMap(recipes => {
          if (recipes.length === 0) {
            this.store.dispatch(new fromRecipeActions.FetchRecipesAction());
            return this.actions$.pipe(
              ofType(RecipeActionTypes.SET_RECIPES),
              take(1)
            );
          } else {
            return of(recipes);
          }
        }));
  }
}
