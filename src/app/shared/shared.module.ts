import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DropdownDirective} from './dropdown.directive';
import {LoadingSpinnerComponent} from './loading-spinner/loading-spinner.component';
import {AlertBoxComponent} from './alert-box/alert-box.component';
import {PlaceHolderDirective} from './place-holder.directive';


@NgModule({
  declarations: [
    DropdownDirective,
    LoadingSpinnerComponent,
    AlertBoxComponent,
    PlaceHolderDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DropdownDirective,
    LoadingSpinnerComponent,
    AlertBoxComponent,
    PlaceHolderDirective,
    CommonModule
  ],
  entryComponents: [
    AlertBoxComponent
  ]
})
export class SharedModule {
}
