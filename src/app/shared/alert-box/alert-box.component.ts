import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-alert-box',
  templateUrl: './alert-box.component.html',
  styleUrls: ['./alert-box.component.css']
})
export class AlertBoxComponent implements OnInit {

  @Input('message') message: string;
  @Output() closeEvent = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  onClose() {
      this.closeEvent.emit();
  }
}
