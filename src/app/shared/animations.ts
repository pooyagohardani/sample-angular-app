import {animate, state, style, transition, trigger} from '@angular/animations';

export const listItemTrigger =
  trigger('listItemState', [
    state('in',
      style({
        opacity: 1,
        transform: 'translateY(0px)'
      })),
    transition('void => *', [
      style({
        opacity: 0,
        transform: 'translateY(-100px)'
      }),
      animate(300)
    ]),
    transition('* => void', [
      animate(300, style({
        opacity: 0,
        transform: 'translateX(+100px)'
      }))
    ])
  ]);

export const fieldToListTrigger =
  trigger('fieldToListState', [
    state('dirty',
      style({
        opacity: 1,
        transform: 'translateY(0px)'
      })),
    state('submit',
      style({
        opacity: 0,
        transform: 'translateY(100px)'
      })),
    transition('dirty => submit', animate(300)),
    transition('* => dirty', animate(0))
  ])
;
