import {Action} from '@ngrx/store';

export const LOGIN_START = '[Auth] Login Start';
export const AUTHENTICATE_SUCCESS = '[Auth] Authenticate Success';
export const AUTHENTICATE_FAILED = '[Auth] Authenticate Failed';
export const LOGOUT = '[Auth] Logout';
export const SIGNUP_START = '[Auth] Signup Start';
export const CLEAR_ERROR = '[Auth] Clear Error';
export const AUTO_LOGIN = '[Auth] Auto Login';

export class AuthenticateSuccessAction implements Action {
  readonly type = AUTHENTICATE_SUCCESS;

  constructor(public payload:
                {
                  email: string,
                  id: string,
                  token: string,
                  expirationDate: Date
                  redirect: boolean
                }) {
  }
}

export class LogoutAction implements Action {
  readonly type = LOGOUT;

  constructor() {
  }
}

export class LoginStartAction implements Action {
  readonly type = LOGIN_START;

  constructor(public payload: { email: string, password: string }) {
  }
}

export class AuthenticateFailedAction implements Action {
  readonly type = AUTHENTICATE_FAILED;

  constructor(public payload: string) {
  }

}

export class SignupStartAction implements Action {
  readonly type = SIGNUP_START;

  constructor(public payload: { email: string, password: string }) {
  }
}

export class ClearErrorAction implements Action {
  readonly type = CLEAR_ERROR;
}

export class AutoLoginAction implements Action {
  readonly type = AUTO_LOGIN;
}

export type actionTypes =
  AuthenticateSuccessAction
  | AuthenticateFailedAction
  | LogoutAction
  | LoginStartAction
  | SignupStartAction
  | ClearErrorAction
  | AutoLoginAction
  ;
