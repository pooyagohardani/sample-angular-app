import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';

import * as AuthActions from './auth.actions';
import {AuthenticateSuccessAction} from './auth.actions';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {AuthResponseData, AuthService} from '../auth.service';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {UserModel} from '../user.model';


@Injectable()
export class AuthEffects {

  @Effect()
  signupStart = this.actions$.pipe(
    ofType(AuthActions.SIGNUP_START),
    switchMap((signupStartAction: AuthActions.SignupStartAction) => {
      console.log('in signup AuthEffects ....');
      return this
        .httpClient
        .post<AuthResponseData>
        (
          'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=' +
          environment.firebaseApiKey,
          {
            email: signupStartAction.payload.email,
            password: signupStartAction.payload.password,
            returnSecureToken: true
          }
        )
        .pipe(
          tap(responseData => {
            this.authService.setLogoutTimer(+responseData.expiresIn * 1000);
          }),
          map(responseData => {
            console.log('in signup responseData ....');
            return this.handleAuthentication(responseData);
          }),
          catchError(errorResponse => {
            console.log('in signup catchError....');
            return this.handleError(errorResponse);
          }),
        );
    })
  );
  @Effect()
  authLogin = this.actions$.pipe(
    ofType(AuthActions.LOGIN_START),
    switchMap((authStartAction: AuthActions.LoginStartAction) => {
      return this.httpClient
        .post<AuthResponseData>
        ('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' + environment.firebaseApiKey,
          {
            email: authStartAction.payload.email,
            password: authStartAction.payload.password,
            returnSecureToken: true
          }
        ).pipe(
          tap(responseData => {
            this.authService.setLogoutTimer(+responseData.expiresIn * 1000);
          }),
          map(responseData => {
            return this.handleAuthentication(responseData);
          }),
          catchError(errorResponse => {
            return this.handleError(errorResponse);
          }),
        );
    })
  );

  @Effect({dispatch: false})
  authRedirect = this.actions$.pipe(
    ofType(AuthActions.AUTHENTICATE_SUCCESS),
    tap((authenticateSuccessAction: AuthenticateSuccessAction) => {
      if (authenticateSuccessAction.payload.redirect) {
        this.router.navigate(['/']);
      }
    }));

  @Effect()
  autoLogin = this.actions$.pipe(
    ofType(AuthActions.AUTO_LOGIN),
    map(() => {
      const userData: {
        email: string,
        id: string,
        _token: string,
        _tokenExpirationDate: string
      } = JSON.parse(localStorage.getItem('userData'));

      if (!userData) {
        return {type: 'DUMMY'};
      }
      const loadedUser = new UserModel(userData.email, userData.id, userData._token, new Date(userData._tokenExpirationDate));
      if (loadedUser.token) {
        const expirationDuration = loadedUser.expirationDate.getTime() - Date.now();
        // this.userSubject.next(loadedUser);
        this.authService.setLogoutTimer(expirationDuration);
        return new AuthActions.AuthenticateSuccessAction({
          email: loadedUser.email,
          id: loadedUser.id,
          token: loadedUser.token,
          expirationDate: loadedUser.expirationDate,
          redirect: false
        });

        // this.autoLogout();
      }
      return {type: 'DUMMY'};
    }));

  @Effect({dispatch: false})
  authLogout = this.actions$.pipe(
    ofType(AuthActions.LOGOUT),
    tap(() => {
      this.authService.clearLogoutTimer();
      localStorage.removeItem('userData');
      this.router.navigate(['/auth']);
    })
  );

  constructor(private actions$: Actions,
              private httpClient: HttpClient,
              private router: Router,
              private authService: AuthService) {
  }

  private handleAuthentication(responseData: AuthResponseData) {
    const expirationDate = new Date(Date.now() + +responseData.expiresIn * 1000);
    const userModel = new UserModel(responseData.email, responseData.localId, responseData.idToken, expirationDate);
    localStorage.setItem('userData', JSON.stringify(userModel));
    return new AuthActions.AuthenticateSuccessAction({
      email: responseData.email,
      id: responseData.localId,
      token: responseData.idToken,
      expirationDate,
      redirect: true
    });
  }

  private handleError(errorResponse) {

    console.log('in handleError method: ' + errorResponse);

    let errorMessage = 'An unknown error occurred!';
    if (!errorResponse.error || !errorResponse.error.error) {
      return of(new AuthActions.AuthenticateFailedAction(errorMessage));
    }
    switch (errorResponse.error.error.message) {
      case 'EMAIL_EXISTS': {
        errorMessage = 'The email address is already in use by another account.';
        break;
      }
      case 'OPERATION_NOT_ALLOWED' : {
        errorMessage = 'Password sign-in is disabled for this project.';
        break;
      }
      case 'TOO_MANY_ATTEMPTS_TRY_LATER' : {
        errorMessage = 'We have blocked all requests from this device due to unusual activity. Try again later.';
        break;
      }
      case 'EMAIL_NOT_FOUND' : {
        errorMessage = 'There is no user record corresponding to this identifier. The user may have been deleted.';
        break;
      }
      case 'INVALID_PASSWORD' : {
        errorMessage = 'The password is invalid or the user does not have a password.';
        break;
      }
      case 'USER_DISABLED' : {
        errorMessage = 'The user account has been disabled by an administrator.';
        break;
      }
    }
    return of(new AuthActions.AuthenticateFailedAction(errorMessage));
  }

}
