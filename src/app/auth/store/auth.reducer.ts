import {UserModel} from '../user.model';
import * as fromAuthActions from '../store/auth.actions';

export const authFeatureKey = 'auth';

export interface State {
  user: UserModel;
  authError: string;
  loading: boolean;
}

export const initialState: State = {
  user: null,
  authError: null,
  loading: false
};

export function reducer(state = initialState, action: fromAuthActions.actionTypes): State {
  switch (action.type) {
    case  fromAuthActions.SIGNUP_START:
    case fromAuthActions.LOGIN_START:
      return {
        ...state,
        authError: null,
        loading: true
      };
    case fromAuthActions.AUTHENTICATE_SUCCESS:
      // const loginAction = action as LoginAction;
      const payload = action.payload;
      const user = new UserModel(payload.email, payload.id, payload.token, payload.expirationDate);
      return {
        ...state,
        user,
        authError: null,
        loading: false
      };
    case fromAuthActions.AUTHENTICATE_FAILED:
      return {
        ...state,
        user: null,
        authError: action.payload,
        loading: false
      };
    case fromAuthActions.LOGOUT:
      return {
        ...state,
        user: null,
        authError: null
      };
    case fromAuthActions.CLEAR_ERROR:
    case fromAuthActions.AUTO_LOGIN:
      return {
        ...state,
        authError: null
      };

    default:
      return state;
  }
}
