import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {AlertBoxComponent} from '../shared/alert-box/alert-box.component';
import {PlaceHolderDirective} from '../shared/place-holder.directive';
import * as fromApp from '../app.reducer';
import {Store} from '@ngrx/store';
import * as fromAuthActions from '../auth/store/auth.actions';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {

  isLoginMode = true;
  isLoading = false;
  error: string;
  closeSubscription: Subscription;
  storeSubscription: Subscription;

  @ViewChild(PlaceHolderDirective, {static: false}) alertHost: PlaceHolderDirective;

  constructor(private authService: AuthService,
              private router: Router,
              private componentFactoryResolver: ComponentFactoryResolver,
              private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.storeSubscription = this.store.select('auth').subscribe(authState => {
      this.isLoading = authState.loading;
      this.error = authState.authError;
      if (this.error) {
        this.showErrorAlert(this.error);
      }
    });
  }

  ngOnDestroy(): void {
    if (this.closeSubscription) {
      this.closeSubscription.unsubscribe();
    }
    if (this.storeSubscription) {
      this.storeSubscription.unsubscribe();
    }
  }


  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
    this.error = '';
  }

  onSubmit(f: NgForm) {
    if (f.invalid) {
      return;
    }
    this.isLoading = true;
    const email = f.value.email;
    const password = f.value.password;
    this.error = '';
    if (this.isLoginMode) {
      // this.authObservable = this.authService.login(email, password);
      this.store.dispatch(new fromAuthActions.LoginStartAction({email, password}));
    } else {
      // this.authObservable = this.authService.signUp(email, password);
      this.store.dispatch(new fromAuthActions.SignupStartAction({email, password}));
    }
    f.reset();
  }

  onHandleError() {
    this.store.dispatch(new fromAuthActions.ClearErrorAction());
  }

  private showErrorAlert(errorMessage: string) {

    const alertComponentFactory = this.componentFactoryResolver.resolveComponentFactory(AlertBoxComponent);
    const viewContainerRef = this.alertHost.viewContainerRef;
    viewContainerRef.clear();
    const alertBoxComponent = viewContainerRef.createComponent(alertComponentFactory);

    alertBoxComponent.instance.message = errorMessage;
    this.closeSubscription = alertBoxComponent.instance.closeEvent.subscribe(
      () => {
        viewContainerRef.clear();
        this.closeSubscription.unsubscribe();
      }
    );

  }
}
