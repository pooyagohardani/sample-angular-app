import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromApp from '../app.reducer';
import * as fromAuth from '../auth/store/auth.actions';

export interface AuthResponseData {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  tokenExpirationTimer: any;

  constructor(private httpClient: HttpClient, private  router: Router, private store: Store<fromApp.AppState>) {
  }

  setLogoutTimer(expirationPeriod: number) {
    this.tokenExpirationTimer = setInterval(() => {
      this.store.dispatch(new fromAuth.LogoutAction());
    }, expirationPeriod);
  }

  clearLogoutTimer() {
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
      this.tokenExpirationTimer = null;
    }
  }
}
