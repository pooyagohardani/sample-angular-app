import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromApp from './app.reducer';
import * as fromAuthActions from './auth/store/auth.actions';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('listItemState', [
        state('in',
          style({
            opacity: 1,
            transform: 'translateX(0px)'
          })),
        transition('void => *', [
          style({
            opacity: 0,
            transform: 'translateX(-100px)'
          }),
          animate(300)
        ]),
        transition('* => void', [
          animate(300, style({
            opacity: 0,
            transform: 'translateX(+100px)'
          }))
        ])
      ])
  ]
})
export class AppComponent implements OnInit {

  constructor(private store: Store<fromApp.AppState>) {

  }

  status = true;
  logs = [];
  index = 0;
  selectedFeature;

  ngOnInit(): void {
    // this.authService.autoLogin();
    this.store.dispatch(new fromAuthActions.AutoLoginAction());
  }
}
